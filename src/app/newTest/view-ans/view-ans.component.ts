import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-view-ans',
  templateUrl: './view-ans.component.html',
  styleUrls: ['./view-ans.component.css']
})
export class ViewAnsComponent implements OnInit {
  langLD: any;
  quesID: any;
  questionArray: any;
  ansArray: any;
  constructor(
    private router:Router,
    private route:ActivatedRoute) { }

  ngOnInit(): void { 
    this.route.paramMap.subscribe(params=>{
    this.langLD= params.get('lang')
    this.quesID= params.get('quesId')
  })
  console.log(this.langLD,"langID_inand")
  console.log(this.quesID,"ques_id_in_ans")
  this.questionArray= this.route.snapshot.data.selectLang;
  console.log(this.questionArray,"this.questionArray_popopo")
  this.ansArray=this.questionArray.filter((data: { ques_id: any; }) =>{ return data.ques_id ===this.quesID })
  console.log(this.ansArray,"ansered arrayyyy")
  }

}
