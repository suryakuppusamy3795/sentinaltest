import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import * as EventEmitter from 'node:events';

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.css']
})
export class ViewPageComponent implements OnInit {
  public questionArray: any;
  public ansArr:any=[];
  @Output() answerArr= new EventEmitter();
  langLD: any;

  constructor(
    private router:Router,
    private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      this.langLD= params.get('lang')
    })
    this.questionArray= this.route.snapshot.data.selectLang;
    console.log(this.questionArray,"this.questionArray_popopo")
console.log(this.langLD,"langLDlangLDlangLDlangLDlangLDlangLDlangLD")
  }
getAnswer(quesId:any){
  
  this.router.navigate([`viewAns/${this.langLD}/${quesId}`])
}
}
