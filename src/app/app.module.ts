import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './newTest/home/home.component';
import { ViewPageComponent } from './newTest/view-page/view-page.component';
import { ViewAnsComponent } from './newTest/view-ans/view-ans.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    HomeComponent,
    ViewPageComponent,
    ViewAnsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    // RouterModule.forRoot([
    //   // { path: '', redirectTo: 'home', pathMatch: 'full' },
    //   {
    //   path:"newHome",
    //   loadChildren:"./newTest/home/home.module#HomeModule"
    // }])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
