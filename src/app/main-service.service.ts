import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  constructor(private http:HttpClient,) { }
  getQuesData():Observable<any>{
    console.log("qwqqwqwqwqwqw")
     return this.http.get(`${environment.baseapi}/getData`)
   }
   getSelectedQues(lang:any):Observable<any>{
    console.log(lang,"qwqqwqwqwqwqw")
     return this.http.get(`${environment.baseapi}/getQues/`+lang)
   }
}
