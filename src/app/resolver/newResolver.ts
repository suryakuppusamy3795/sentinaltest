import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {
    Router,
    Resolve,
    RouterStateSnapshot,
    ActivatedRouteSnapshot,
    ActivatedRoute
} from '@angular/router';
import { MainServiceService } from '../main-service.service';


@Injectable()
export class quesResolver implements Resolve<any> {
    constructor(
        private mainService: MainServiceService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> {
        // const id = route.queryParams['id'];
        return this.mainService.getQuesData();
    }
}

@Injectable()
export class selectLang implements Resolve<any> {
    constructor(
        private mainService: MainServiceService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> {
        var langLD= route.paramMap.get('lang')?route.paramMap.get('lang'):""
        console.log(route.paramMap.get('lang'),"langlanglanglanglanglanglang")
        // const id = route.queryParams['id'];
        return this.mainService.getSelectedQues(langLD);
    }
}