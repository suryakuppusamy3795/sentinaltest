import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
public langArray:any = []
  constructor(
    private router:Router,
    private route:ActivatedRoute
  ) { }

  ngOnInit(): void {
    console.log("hitted home page")
this.langArray= this.route.snapshot.data.quersResolver;
console.log(this.langArray,"langArrayyyy")
  }
  getQues(lngNm:string){
console.log(lngNm,"selected_lang_name")
this.router.navigate([`viewQues/${lngNm}`])
  }
}
