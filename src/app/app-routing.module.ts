import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { ViewAnsComponent } from './newTest/view-ans/view-ans.component';
import { ViewPageComponent } from './newTest/view-page/view-page.component';
import { quesResolver, selectLang } from './resolver/newResolver';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
  path:"home",
  component:HomepageComponent,
resolve:{
  quersResolver:quesResolver
},
},{
  path:"viewQues/:lang",
  component:ViewPageComponent,
resolve:{
  selectLang:selectLang
},
},{
  path:"viewAns/:lang/:quesId",
  component:ViewAnsComponent,
  resolve:{
    selectLang:selectLang
  },
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[quesResolver,selectLang]
})
export class AppRoutingModule { }
